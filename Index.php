<?php
 session_start();
$bdd = new PDO('mysql:host=localhost;dbname=RUATNOVAIS;charset=utf8', 'root', '');

/* Comment recuperer le résultat d'une requete SQL en PHP
$reponse = $bdd->query("SELECT * FROM produits ;");
$data = $reponse->fetch();
echo $data['Nom_Produit'];
*/
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8"/>
        <link rel="stylesheet" href="style.css"/>
        <title>Vendeur de jeux.COM</title>
    </head>
    <body>    
    <?php include("Header.php"); ?>

    <div id="main">
        <p class="texte">
        </p>
    </div>

    <section class="products">
        <!-- <a href="GamePage.php?idgame=<?php echo $produit['ID_Produit'] ?>"> -->
            <?php
                $reponse = $bdd->query("SELECT * FROM produits ORDER BY RAND() LIMIT 5");
                while($produit = $reponse->fetch())
                { 
            ?>
            <div class="product-card">
                <a href="GamePage.php?idgame=<?php echo $produit['ID_Produit'] ?>">
            <div class="product-image">
                <?php
                    echo '<img src="'.$produit['Image_Produit'].'"/>';
                ?>
            </div>
            <div class="product-info">
                <?php
                    echo '<h5>'.$produit['Nom_Produit'].'</h5>';
                    echo '<h6>'.$produit['Prix_Produit'].'€</h6>';
                ?>
            </div>
            </div>
            <?php
            
                }
            ?> 
            </a>
            
    </section>
    <?php include("Footer.php"); ?> 
    </body>
</html>