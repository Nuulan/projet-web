<?php
    session_start();
    try {
    $bdd = new PDO('mysql:host=localhost;dbname=RUATNOVAIS;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch(Exception $e)
    {
        die($e->getMessage());
    }
    ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link rel="stylesheet" href="Style.css" />
</head>

<body>
    <?php include("Header.php"); ?>

    <?php
        if(isset($_POST["Adresse"]) && isset($_POST["Nom"]) && isset($_POST["Prenom"]) && isset($_POST["DDN"]) && isset($_POST["CP"]) && isset($_POST["Ville"]) && isset($_POST["Identifiant"]) && isset($_POST["Passe"]))
        {
            function genererChaineAleatoire($longueur)
            {
                $chaine = '';
                $listeCar = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $max = strlen($listeCar);
                $max = $max -1;
                for ($i = 0; $i < $longueur; ++$i) {
                    $chaine .= $listeCar[random_int(0, $max)];
                }
                return $chaine;
            }
            $nom = $_POST["Nom"];
            $prenom = $_POST["Prenom"];
            $adresse = $_POST["Adresse"];
            $cp = $_POST["CP"];
            $ville = $_POST["Ville"];
            $login = $_POST["Identifiant"];
            $ddn = $_POST["DDN"];
            $mdp = $_POST["Passe"];
            $sel = genererChaineAleatoire(30);
            $req = $bdd->prepare("INSERT INTO clients(Nom_client, Prenom_client, Adresse_client, CP_client, Ville_client,
                DDN_client, Login_client, Empreinte_client, Sel_client) VALUES (:nom_client,:prenom_client,:adresse_client,:cp_client,:ville_client,:ddn_client,:login_client,:empreinte_client,:sel_client)");
            $req->execute(array(
            "nom_client" => $nom,
            "prenom_client" => $prenom,
            "adresse_client" => $adresse,
            "cp_client" => $cp,
            "ville_client" => $ville,
            "login_client" => $login,
            "ddn_client" => $ddn,
            "empreinte_client" => hash("sha256",$mdp.$sel),
            "sel_client" => $sel));
        }
        if (isset($_POST["Identifiant"]))
        {
            $login = $_POST["Identifiant"];
            $req = $bdd->prepare("SELECT Sel_client FROM clients WHERE Login_client=:login_client");
            $req2 = $req->execute(array("login_client" => $login));
            $sel = $req->fetch();
            if ($sel == false)
            {
                ?>
                <div id="errorlogin">
                    <h3>&#9888 Erreur de connexion</h3>
                    <p>Les identifiants sont incorrects !</p>
                </div>
                <?php
            }
            else
            {
                $mdp = $_POST["Passe"];
                $req = $bdd->prepare("SELECT Login_client,ID_client FROM clients WHERE Login_client=:pseudo AND Empreinte_client=:empreinte");
                $req->bindParam(':pseudo',$login);
                $hash = hash("sha256",$mdp.$sel["Sel_client"]);
                $req->bindParam(':empreinte',$hash);
                $req->execute();
                $reqlogin = $req->fetch();
                if($reqlogin == false)
                {
                    ?>
                    <div id="errorlogin">
                        <h3>&#9888 Erreur de connexion</h3>
                        <p>Les identifiants sont incorrects !</p>
                    </div>
                    <?php
                }
                else
                {
                    $_SESSION["ID"] = $reqlogin["ID_client"];
                    $_SESSION["Login_client"] = $reqlogin["Login_client"];
                    header('Location: /FINAL/TP_Final/Profile.php');
                }
            }
        }
        if (isset($_SESSION["ID"]))
        {
            $id = $_SESSION["ID"];
            $req = $bdd->prepare("SELECT * FROM clients WHERE ID_client=:id");
            $req->execute(array("id" => $id));
            $res = $req->fetch();
            ?>
            <div class="profileInfoBloc">
                <div id="partie1" class="info">
                    <h1>Informations de compte</h1>
                    <p>Nom de compte : <?php echo $res["Login_Client"] ?></p>
                </div>
                <div id="partie2" class="info">
                    <h1>Informations personnelles</h1>
                    <p>Addresse : <?php echo $res[3].", ".$res[4].", ".$res[5] ?></p>
                </div>
            </div>
            <?php
        }
        else
        {
            ?>
            <div id="errorlogin">
                <p>Vous n'êtes pas connecté !</p>
                <a id="inscription" href="Connexion.php">Vous pouvez vous connecter ICI</a><p></p>
                <a id="inscription" href="Inscription.php">Ou créer votre compte ICI</a><p></p>
            </div>
            <?php

        }
    ?>






        <?php include("Footer.php"); ?>
</body>

</html>