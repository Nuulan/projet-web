<?php
    session_start();
    $bdd = new PDO('mysql:host=localhost;dbname=RUATNOVAIS;charset=utf8', 'root', '');
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if(isset($_POST))
    {
        $sql = "SELECT * FROM produits";
        if(isset($_POST['selectFiltre']) && $_POST['selectFiltre'] != "1")
            $sql .= " WHERE ID_Categorie = ".$_POST['selectFiltre'];
        if(isset($_POST['select']))
        {
            $sql .= " ORDER BY ";
            switch($_POST['select'])
            {
                case 'alphabet':
                    $sql .= "Nom_Produit ASC";
                    break;
                case 'dateTri':
                    $sql .= "Date_Produit ASC";
                    break;
                case "prixCroi":
                    $sql .= "Prix_Produit ASC";
                    break;
                case "prixDecroi":
                    $sql .= "Prix_Produit DESC";
                    break;
            }
        } 
        $response = $bdd->query($sql);
        $produits = $response->fetchAll();
    }

    $reponse = $bdd->query("SELECT * FROM categories;");
    $categories = $reponse->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <?php include("Header.php"); ?>

    <div class="main">
    <nav class="product-filter">
        <div class="sort">
            <form method="post" action="">
              <div class="collection-sort">
                <label>Filtrer par:</label>
                    <select name="selectFiltre">
                        <option value="1">Tout les jeux </option>
                        <option value="2"> PC </option>
                        <option value="3"> XBOX ONE </option>
                        <option value="4"> PS4</option>
                        <option value="5"> Nintendo Switch</option>
                    </select>
            </div>
            <div class="collection-sort">

                    <label>Trier par:</label>
                    <select name="select">
                        <option value="alphabet">Alphabet</option>
                        <option value="dateTri">Date</option>
                        <option value="prixCroi">Prix Croissant</option>
                        <option value="prixDecroi">Prix Décroissant</option>
                    </select>
                    <div class="button">
                        <button type="submit">Envoyer le Tri</button>
                    </div>
            </div>
          </form>
        </div>
    </nav>
        
        <section class="products">  
            <?php
                foreach($produits as $produit)
                {
            ?>
            <div class="product-card">
                <a href="GamePage.php?idgame=<?php echo $produit['ID_Produit'] ?>">
            <div class="product-image">
                <?php
                    echo '<img src="'.$produit['Image_Produit'].'"/>';
                ?>
            </div>
            <div class="product-info">
                <?php
                    echo '<h5>'.$produit['Nom_Produit'].'</h5>';
                    echo '<h6>'.$produit['Prix_Produit'].'€</h6>';
                ?>
            </div>
            </div>
            <?php
            
                }
            ?> 
            </a>    
        </section>
    </div>
    <?php include("Footer.php"); ?>    
</body>
</html>