<?php
    session_start();
    $bdd = new PDO('mysql:host=localhost;dbname=RUATNOVAIS;charset=utf8', 'root', '');
    
    $reponse = $bdd->query("SELECT * FROM produits WHERE ID_Categorie = ".$_GET['Categorie']." ORDER BY Nom_Produit ASC");
    $produits = $reponse->fetchAll();

    $reponse = $bdd->query("SELECT * FROM categories;");
    $categories = $reponse->fetchAll();

    if(isset($_POST['selectTri']))
        switch($_POST['selectTri'])
        {
            case "alphabet":
                $reponse = $bdd->query("SELECT * FROM produits WHERE ID_Categorie = ".$_GET['Categorie']." ORDER BY Nom_Produit ASC");
                $produits = $reponse->fetchAll();
            break;

            case "dateTriCroi":
                $reponse = $bdd->query("SELECT * FROM produits WHERE ID_Categorie = ".$_GET['Categorie']." ORDER BY Date_Produit ASC");
                $produits = $reponse->fetchAll();
            break;

            case "dateTriDecroi":
                $reponse = $bdd->query("SELECT * FROM produits WHERE ID_Categorie = ".$_GET['Categorie']." ORDER BY Date_Produit DESC");
                $produits = $reponse->fetchAll();
            break;
            case "prixCroi":
                $reponse = $bdd->query("SELECT * FROM produits WHERE ID_Categorie = ".$_GET['Categorie']." ORDER BY Prix_Produit ASC");
                $produits = $reponse->fetchAll();
            break;
            case "prixDecroi":
                $reponse = $bdd->query("SELECT * FROM produits WHERE ID_Categorie = ".$_GET['Categorie']." ORDER BY Prix_Produit DESC");
                $produits = $reponse->fetchAll();
            break;
        }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <?php include("Header.php"); ?>

    <div class="main">
    <nav class="product-filter">
        <div class="sort">
            <div class="collection-sort">
                <label>Filtrer par:</label>
                <select name="selectFiltre">
                <option value="all">Tout les jeux </option>
                <option value="PC"> PC </option>
                <option value="XBOX_ONE"> XBOX ONE </option>
                <option value="PS4"> PS4</option>
                <option value="Nintendo_Switch"> Nintendo Switch</option>
                <option value="date">Date </option>
                </select>
            </div>
            <div class="collection-sort">
                <form method="post">
                    <label>Trier par:</label>
                    <select name="selectTri">
                        <option value="alphabet">Alphabet</option>
                        <option value="dateTriCroi">Plus vieux</option>
                        <option value="dateTriDecroi">Plus jeune</option>
                        <option value="prixCroi">Prix Croissant</option>
                        <option value="prixDecroi">Prix Décroissant</option>
                    </select>
                    <div class="button">
                            <button type="submit">Envoyer le Tri</button>
                    </div>
                </form>
            </div>

        </div>
        </nav>

        <section class="products">
            <?php
                foreach($produits as $produit)
                {
            ?>
            <div class="product-card">
                <a href="GamePage.php?idgame=<?php echo $produit['ID_Produit'] ?>">
                <div class="product-image">
                    <?php
                        echo '<img src="'.$produit['Image_Produit'].'"/>';
                    ?>
                </div>
                <div class="product-info">
                    <?php
                        echo '<h5>'.$produit['Nom_Produit'].'</h5>';
                        echo '<h6>'.$produit['Prix_Produit'].'€</h6>';
                    ?>
                </div>
            </div>
            <?php
            
                }
            ?> 
            </a>
        </section>
    </div>


    <?php include("Footer.php"); ?>    
</body>
</html>