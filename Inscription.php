<?php
    session_start();
    $bdd = new PDO('mysql:host=localhost;dbname=RUATNOVAIS;charset=utf8', 'root', '');
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <?php include("Header.php"); ?>
    <div id="main">
        <div class="inscri" id="formulaire">
            <h1>Création de compte</h1>
            <form action="Profile.php" method="post">
                <h2>Données Personnelle</h2>
                <p>Nom : <input type="text" name="Nom"/></p>
                <p>Prénom : <input type="text" name="Prenom"/></p>
                <p>Date de naissance : <input type="date" name="DDN" max="2020-05-01"/></p>
                <p>Addresse : <input type="text" name="Adresse"/></p>
                <p>Code Postal : <input type="text" name="CP"/></p>
                <p>Ville : <input type="text" name="Ville"/></p>
                <h2>Données de Compte</h2>
                <p>Identifiant : <input type="text" name="Identifiant"/></p>
                <p>Mot de passe : <input type="password" name="Passe"/></p>
                <p></p><input type="submit" value="S'inscrire"/>
            </form>
        </div>
    </div>   
     <?php include("Footer.php"); ?>    
</body>
</html>